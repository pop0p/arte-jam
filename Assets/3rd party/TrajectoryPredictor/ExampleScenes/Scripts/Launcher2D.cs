﻿using UnityEngine;
using UnityEngine.UI;

public class Launcher2D : MonoBehaviour
{

    public GameObject objToLaunch;
    public Transform launchPoint;
    public Text infoText;
    public bool launch;
    [Range(5, 45)]
    public float forceTiming = 150f;
    [Range(5, 45)]
    public float force = 50f;
    public float moveSpeed = 1f;

    public bool ViewTimingPredict;
    public Sprite OnSprite;
    public Sprite OffSprite;
    private SpriteRenderer _sr;

    private float _currentCD;

    //create a trajectory predictor in code
    TrajectoryPredictor tp;
    void Start()
    {
        tp = gameObject.AddComponent<TrajectoryPredictor>();
        tp.predictionType = TrajectoryPredictor.predictionMode.Prediction2D;
        tp.drawDebugOnPrediction = true;
        tp.accuracy = 0.99f;
        tp.lineWidth = 0.025f;
        tp.iterationLimit = 300;
        tp.lineStartColor = new Color32(255, 255, 255, 50);
        tp.lineEndColor = new Color32(255, 255, 255, 50);
        _sr = GetComponentInChildren<SpriteRenderer>();
        _sr.sprite = OnSprite;

    }

    // Update is called once per frame
    void Update()
    {
        //set line duration to delta time so that it only lasts the length of a frame
        tp.debugLineDuration = Time.unscaledDeltaTime;
        //tell the predictor to predict a 2d line. this will also cause it to draw a prediction line
        //because drawDebugOnPredict is set to true
        if (ViewTimingPredict)
            tp.Predict2D(launchPoint.position, launchPoint.right * forceTiming, Physics2D.gravity, objToLaunch.GetComponent<Rigidbody2D>().drag);
        else
            tp.Predict2D(launchPoint.position, launchPoint.right * force, Physics2D.gravity, objToLaunch.GetComponent<Rigidbody2D>().drag);

        //this static method can be used as well to get line info without needing to have a component and such
        //TrajectoryPredictor.GetPoints2D(launchPoint.position, launchPoint.right * force, Physics2D.gravity);


        //info text stuff
        if (infoText)
        {
            //this will check if the predictor has a hitinfo and then if it does will update the onscreen text
            //to say the name of the object the line hit;
            if (tp.hitInfo2D)
                infoText.text = "Hit Object: " + tp.hitInfo2D.collider.gameObject.name;
        }

        //if (GameManager.IsInTiming)

        _currentCD -= Time.deltaTime;
    }

    public void Launch(Rigidbody2D rbi, bool success, bool timing, float CD)
    {
        if (_currentCD > 0)
            return;

        _currentCD = CD;
        rbi.velocity = Vector2.zero;
        if (success)
        {
            rbi.velocity = launchPoint.right * forceTiming;
            if (timing)
            GameManager._instance.AddTimer();
        }
        else
        {
            rbi.velocity = launchPoint.right * force;
        }
        rbi.GetComponent<Player>().StartYInteraction(rbi.GetComponent<Player>().Options.BidouOptions.BidouInputBlockDelay);
    }
}
