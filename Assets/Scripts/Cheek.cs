﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheek : MonoBehaviour
{
    public SpriteRenderer SpriteRendererAnneau;
    public SpriteRenderer SpriteRendererCoeur;
    public Vector3 MaxScale;
    public Vector3 MinScale;
    public float ScaleLength;
    private int _currentScaleDirection = 1;

    private float _currentTime;

    public AudioClip[] Audios;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        SpriteRendererAnneau.transform.localScale = MinScale;
    }

    // Update is called once per frame
    void Update()
    {
        var perc = _currentTime / ScaleLength;
        var to = MaxScale;
        if (_currentScaleDirection == 1)
            to = MaxScale;
        else
            to = MinScale;

        var from = MinScale;
        if (_currentScaleDirection == 1)
            from = MinScale;
        else
            from = MaxScale;

        SpriteRendererAnneau.transform.localScale = Vector3.Lerp(from, to, perc);
        if (perc >= 1)
        {
            _currentScaleDirection = -_currentScaleDirection;
            _currentTime = 0;
        }

        _currentTime += Time.deltaTime;

    }

    public void PlaySound()
    {
        _audioSource.PlayOneShot(Audios[Random.Range(0, Audios.Length)]);
    }
}
