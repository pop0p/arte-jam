﻿using UnityEngine;

public class FollowPlayerOnY : MonoBehaviour
{
    private GameObject _target;
    private Transform _targetTrans;

    public float smoothSpeed;
    public float YOffset;


    private bool falling;
    private void Start()
    {


    }

    private void LateUpdate()
    {
        if (FindObjectOfType<Player>())
        {
            _target = FindObjectOfType<Player>().gameObject;
            _targetTrans = _target.transform;
        } else
        {
            return;
        }
        
        Vector3 target = new Vector3(0, _targetTrans.position.y + YOffset, -10);

        if (_target.GetComponent<Rigidbody2D>().velocity.y < 0 && !falling)
        {
            falling = true;
            smoothSpeed += 5;
        }
        else if (_target.GetComponent<Rigidbody2D>().velocity.y >= 0 && falling)
        {
            falling = false;
            smoothSpeed -= 5;
        }
        Vector3 smoothed = Vector3.Lerp(transform.position, target, smoothSpeed * Time.deltaTime) ;
        transform.position = smoothed;
    }
}
