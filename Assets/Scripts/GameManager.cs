﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private Animator _animtor;
    public static GameManager _instance;
    public GameOptions Options;
    public GameOptions _options;
    private Text _timerText;
    private float _currentSecondTimer;
    private static bool _isInTiming;
    public static bool IsInTiming { get { return _isInTiming; } }
    public Vector3 lastCheckPointPos;
    private int _levelToLoad;
    public bool StickAt7;
    private AudioSource mainThemeAudioSource;
    public int TargetFramerate;

    public bool IsInMenu;
    public bool IsPaused;
    public GameObject PauseMenu;
    public GameObject MenuMenu;
    public GameObject InGamePlayer;

    public Button PlayBtn;
    public Button QuitBtn;

    private bool _end;

    private void Awake()
    {

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.vSyncCount = 0;
    }


    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    IEnumerator merde()
    {
        yield return new WaitForSeconds(.5f);
        InGamePlayer.SetActive(false);

        yield return new WaitForSeconds(.5f);
        InGamePlayer.SetActive(true);

    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (_levelToLoad == 0) IsInMenu = true;
        else
            IsInMenu = false;

        if (IsInMenu)
        {
            PlayBtn.onClick.AddListener(delegate () { FadeToLevel(1); });
            QuitBtn.onClick.AddListener(delegate () { Quit(); });
            MenuMenu.SetActive(true);
            lastCheckPointPos = Vector3.zero;
            StartCoroutine(merde());

        } else
        {
            MenuMenu.SetActive(false);
        }
        mainThemeAudioSource = GetComponent<AudioSource>();
        CancelInvoke("ReduceTimer");
        _animtor = GetComponent<Animator>();
        _animtor.SetTrigger("Fadeint");
        _options = ScriptableObject.CreateInstance("GameOptions") as GameOptions;
        _options.Timer = Options.Timer;
        _options.GoodTimingTimerBonus = Options.GoodTimingTimerBonus;
        if (GameObject.FindGameObjectWithTag("TextTimer"))
            _timerText = GameObject.FindGameObjectWithTag("TextTimer").GetComponent<Text>();
        _options.Timer += 1;
        mainThemeAudioSource.Play();
        _end = false;
    }

    // Update is called once per frame
    void Update()
    {
#if DEBUG
        Application.targetFrameRate = TargetFramerate;
#endif
        if (IsInMenu || IsPaused)
            return;

        if (StickAt7)
        {
            _options.Timer = 7;
            DisplayTimer();
            return;
        }

        if (_currentSecondTimer >= .6 && _currentSecondTimer < 1)
            _isInTiming = true;
        else
            _isInTiming = false;

        if (_currentSecondTimer >= 1)
        {
            _currentSecondTimer = 0;
            ReduceTimer();
        }

        _currentSecondTimer += Time.deltaTime;

        if (_isInTiming)
            _timerText.color = Color.green;
        else
            _timerText.color = Color.red;
    }
    void ReduceTimer()
    {
        if (StickAt7)
            return;

        _options.Timer--;
        DisplayTimer();

        if (_options.Timer <= 0 && !_end)
        {
            _end = true;
            FadeToLevel(1);
        }
    }

    public void AddTimer()
    {
        _options.Timer += _options.GoodTimingTimerBonus;
        if (_options.Timer > 7)
            _options.Timer = 7;
        DisplayTimer();

    }

    void DisplayTimer()
    {
        _timerText.text = _options.Timer.ToString();
    }

    public void FadeToLevel(int levelIndex)
    {
        Debug.Log("Fin, fade out.");
        CancelInvoke("ReduceTimer");
        _levelToLoad = levelIndex;
        if (_levelToLoad == 0) { IsInMenu = true; IsPaused = false; PauseMenu.SetActive(IsPaused); }
        _animtor.SetTrigger("Fadeout");

    }

    public void OnFadeComplete()
    {
        Debug.Log("Fin complete");
        SceneManager.LoadScene(_levelToLoad);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
      Application.Quit();
#endif
    }

    public void Pause()
    {
        if (IsInMenu) return;
        if (IsPaused)
        {
            IsPaused = false;
        }
        else
        {
            IsPaused = true;
        }
        Debug.Log(IsPaused);
        PauseMenu.SetActive(IsPaused);
    }
}
