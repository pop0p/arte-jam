﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class GameOptions : ScriptableObject
{
    public float Timer;
    public int GoodTimingTimerBonus;

}
