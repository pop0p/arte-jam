﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    #region Inputs Fields
    private bool _jumpPressed;
    private bool _bidouPressed;
    private bool _buttPressed;
    private bool _softnessPressed;
    private bool _cheekPressed;
    private Vector2 _movementsInput;
    #endregion

    public static Player _instance;

    public List<Button> _buttons;
    public int _selectedBtns = 0;

    private Rigidbody2D _rb2d;
    [SerializeField] public PlayerOptions Options;

    public bool YInteractionJustStarted;

    // bidou
    private float _currentBidouCD;
    //butt
    private float _currentButtCD;
    private float _currentButtLength;
    private float _initGravity;
    private float _initMaxSpeed;
    private bool _isButting;
    //softness
    private float _currentSoftnessCD;
    private Softness _closeSofter;
    private Softness _currentSofter;
    private float _maximumTempsDaccro = 100;
    //cheek
    private float _currentCheekCD;
    private bool _isInited = false;


    public Vector3 StartPosition;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        if (!GameManager._instance || GameManager._instance.IsInMenu)
        {
            _selectedBtns = 0;
            var x = GameObject.FindGameObjectsWithTag("MenuBtn");
            if (x.Count() > 0)
            {
                Debug.Log("Ajout des boutons");
                _buttons = x.Select(t => t.GetComponent<Button>()).ToList();
                _buttons[_selectedBtns].Select();
            }
        }
        else
            Init();

    }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else
            Destroy(gameObject);
    }
    private void OnLevelWasLoaded(int level)
    {
        Init();
    }
    private void OnEnable()
    {
        if (!GameManager._instance || GameManager._instance.IsInMenu)
        {
            _selectedBtns = 0;
            var x = GameObject.FindGameObjectsWithTag("MenuBtn");
            if (x.Count() > 0)
            {
                Debug.Log("Ajout des boutons");
                _buttons = x.Select(t => t.GetComponent<Button>()).ToList();
                _buttons[_selectedBtns].Select();
            }
        }
        else
            Init();
    }

    void Init()
    {
        _selectedBtns = 0;
        var x = GameObject.FindGameObjectsWithTag("MenuBtn");
        if (x.Count() > 0)
        {
            Debug.Log("Ajout des boutons");
            _buttons = x.Select(t => t.GetComponent<Button>()).ToList();
            _buttons[_selectedBtns].Select();
        }

        _rb2d = GetComponent<Rigidbody2D>();
        if (!GameManager._instance.lastCheckPointPos.Equals(Vector3.zero))
            transform.position = GameManager._instance.lastCheckPointPos;
        else
            transform.position = StartPosition;


        _initGravity = _rb2d.gravityScale;
        _initMaxSpeed = Options.MaxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._instance.IsInMenu || GameManager._instance.IsPaused) return;
        else
        {
            if (!_isInited)
            {
                Init();
                transform.position = StartPosition;
                _isInited = true;
            }
        }
        AnimateMovements();

        if (_cheekPressed)
            CheckCollisionWithCheek();
        _currentBidouCD -= Time.deltaTime;
        _currentButtCD -= Time.deltaTime;
        _currentSoftnessCD -= Time.deltaTime;
        _currentCheekCD -= Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (GameManager._instance.IsInMenu || GameManager._instance.IsPaused) return;

        if (_currentSofter == null)
            Movements();
        GetSofted();
    }

    private void CheckCollisionWithCheek()
    {
        Collider2D coll = Physics2D.OverlapCircle(transform.position, .5f, 1 << 9);
        if (coll)
        {
            var x = coll.GetComponentInParent<Cheek>();
            if (x)
            {
                _cheekPressed = false;
                var dir = coll.transform.position - transform.position;
                _rb2d.velocity = Vector2.zero;
                _rb2d.AddForce(dir.normalized * Options.CheekOptions.ThrowForce, ForceMode2D.Impulse);
                x.PlaySound();
                StartYInteraction(Options.CheekOptions.CheekInputBlockDelay);
            }
        }
    }
    #region Inputs Methods

    private void OnJump(InputValue inp)
    {
        _jumpPressed = inp.isPressed;

        if (_jumpPressed)
        {
            StartCoroutine(TurnJumpOff());
        }
        else
        {
            StopCoroutine(TurnJumpOff());
        }
        _jumpPressed = inp.isPressed;
    }
    private void OnBidou(InputValue inp)
    {
        _bidouPressed = inp.isPressed;

        if (_currentBidouCD > 0)
            _bidouPressed = false;

        if (_bidouPressed)
        {
            _currentBidouCD = Options.BidouOptions.BidouCD;

            StartCoroutine(TurnBidouOff());
        }
        else
        {
            StopCoroutine(TurnBidouOff());
        }

    }
    private void OnCheek(InputValue inp)
    {
        _cheekPressed = inp.isPressed;

        if (_currentCheekCD > 0)
            _cheekPressed = false;

        if (_cheekPressed)
        {
            _currentCheekCD = Options.CheekOptions.CheekCD;

            StartCoroutine(TurnCheekOff());
        }
        else
        {
            StopCoroutine(TurnCheekOff());
        }

    }
    private void OnSoftness(InputValue inp)
    {

        _softnessPressed = inp.isPressed;

        if (_currentSoftnessCD > 0)
            _softnessPressed = false;

        if (_softnessPressed)
        {
            StartCoroutine(TurnSoftnessOff());
        }
        else
        {
            StopCoroutine(TurnSoftnessOff());
        }

    }
    private void OnButt(InputValue inp)
    {
        _buttPressed = inp.isPressed;

        if (_currentButtCD > 0)
            _buttPressed = false;

        if (_buttPressed && !IsGrounded())
        {
            _currentButtCD = Options.ButtOptions.ButtCD;
            _isButting = true;
            _currentButtLength = Options.ButtOptions.ButtLength;
            StartCoroutine(TurnButtOff());
        }
        else
        {
            StopCoroutine(TurnButtOff());
        }

    }
    private void OnMove(InputValue inp) => _movementsInput = inp.Get<Vector2>();
    private void OnStart(InputValue inp)
    {
        if (inp.isPressed)
        {
            GameManager._instance.Pause();
            _selectedBtns = 0;
            var x = GameObject.FindGameObjectsWithTag("MenuBtn");
            if (x.Count() > 0)
            {
                Debug.Log("Ajout des boutons");
                _buttons = x.Select(t => t.GetComponent<Button>()).ToList();
                _buttons[_selectedBtns].Select();
            }

        }
    }
    private void OnNavigate(InputValue inp) { }

    #endregion

    IEnumerator TurnBidouOff()
    {
        yield return new WaitForSeconds(Options.BidouOptions.BidouMarge);
        _bidouPressed = false;
    }
    IEnumerator TurnJumpOff()
    {
        yield return new WaitForSeconds(Options.JumpMarge);
        _jumpPressed = false;
    }
    IEnumerator TurnSoftnessOff()
    {
        yield return new WaitForSeconds(Options.SoftnessOptions.SoftnessMarge);
        if (!_currentSofter)
            _softnessPressed = false;
    }
    IEnumerator TurnCheekOff()
    {
        yield return new WaitForSeconds(Options.CheekOptions.CheekMarge);
        _cheekPressed = false;
    }

    IEnumerator TurnButtOff()
    {
        yield return new WaitForSeconds(Options.ButtOptions.ButtMarge);
        _buttPressed = false;
    }
    private void Movements()
    {
        if (!YInteractionJustStarted)
        {
            if (_movementsInput.x < -0.1f && !IsAgainstLeftWall())
            {
                if (_rb2d.velocity.x > -Options.MaxSpeed)
                    _rb2d.AddForce(new Vector2(-Options.Acceleration, 0f));
                else
                    _rb2d.velocity = new Vector2(-Options.MaxSpeed, _rb2d.velocity.y);
            }
            else if (_movementsInput.x > 0.1f && !IsAgainstRightWall())
            {
                if (_rb2d.velocity.x < Options.MaxSpeed)
                    _rb2d.AddForce(new Vector2(Options.Acceleration, 0f));
                else
                    _rb2d.velocity = new Vector2(Options.MaxSpeed, _rb2d.velocity.y);
            }
        }


        if (_jumpPressed && !YInteractionJustStarted)
        {
            if (IsGrounded())
            {
                _rb2d.velocity = new Vector2(_rb2d.velocity.x, Options.JumpVelocity);
                if (GameManager.IsInTiming)
                    GameManager._instance.AddTimer();
            }
        }
        else
        {
            if (IsGrounded() && !YInteractionJustStarted)
            {
                _rb2d.velocity = new Vector2(_rb2d.velocity.x, 0);
            }
        }
        // falling
        if (_isButting && _currentButtLength > 0)
        {
            _rb2d.velocity = new Vector2(_rb2d.velocity.x, 1 * Options.ButtOptions.ButtGravity * (Options.FallMultiplayer));
            _rb2d.gravityScale = Options.ButtOptions.ButtGravity;
            Options.MaxSpeed = Options.ButtOptions.ButtMaxSpeed;
        }
        else
        {
            _rb2d.gravityScale = _initGravity;
            Options.MaxSpeed = _initMaxSpeed;
            _isButting = false;
        }

        if (_rb2d.velocity.y < 0)
        {
            _rb2d.velocity += Vector2.up * Physics2D.gravity.y * _rb2d.gravityScale * (Options.FallMultiplayer) * Time.deltaTime;

        }

        _currentButtLength -= Time.deltaTime;

    }
    private void GetSofted()
    {
        // pull to the center
        if (_softnessPressed && _closeSofter && _maximumTempsDaccro > 0)
        {
            if (_currentSofter == null)
                _maximumTempsDaccro = Options.SoftnessOptions.SoftnessMarge;

            _currentSofter = _closeSofter;
            _rb2d.MovePosition(Vector3.MoveTowards(transform.position, _closeSofter.Target.transform.position, Options.SoftnessOptions.AttractionForce * Time.deltaTime));
            GameManager._instance.StickAt7 = true;

            _maximumTempsDaccro -= Time.deltaTime;
        }
        // throw if at the center, let go if not.
        else if ((_currentSofter && !_softnessPressed) || (_maximumTempsDaccro <= 0 && _currentSofter))
        {
            if (Vector2.Distance(transform.position, _closeSofter.Target.transform.position) < 0.2f)
            {
                _rb2d.velocity = Vector2.zero;
                _rb2d.AddForce(Vector2.up * Options.SoftnessOptions.ThrowForce, ForceMode2D.Impulse);
                StartYInteraction(Options.SoftnessOptions.SoftnessInputBlockDelay);
            }
            GameManager._instance.StickAt7 = false;
            _currentSoftnessCD = Options.SoftnessOptions.SoftnessCD;
            _currentSofter = null;
            _softnessPressed = false;
            _maximumTempsDaccro = 100;

        }
    }

    private void AnimateMovements()
    {
    }

    private bool IsGrounded()
    {
        float extraHeightTest = .1f;
        RaycastHit2D boxcastHit = Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size / 1.2f, 0f, Vector2.down, extraHeightTest, 1 << 8);
        if (boxcastHit.collider != null)
        {
            if (boxcastHit.transform.position.y < transform.position.y)
            {
                // verif supplémentaires car parfois, quand on  est collé à un mur dans le vide, on est considéré comme grounded.
                if (Physics2D.Raycast(transform.position, Vector2.down, .5f, 1 << 8) && (IsAgainstLeftWall() || IsAgainstRightWall()))
                {
                    return true;
                }
                else if (!Physics2D.Raycast(transform.position, Vector2.down, .5f, 1 << 8) && (IsAgainstLeftWall() || IsAgainstRightWall()))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        return false;
        //return transform.Find("GroundCheck").GetComponent<GroundCheck>().IsGrounded;

        //return Physics2D.OverlapArea(new Vector2(transform.position.x - 0.5f, transform.position.y - 0.48f), new Vector2(transform.position.x + 0.5f, transform.position.y - 0.5f), 1 << 8);
    }

    private bool IsAgainstLeftWall()
    {
        float extraWidthTest = .1f;
        RaycastHit2D boxcastHit = Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.left, extraWidthTest, 1 << 8);
        if (boxcastHit.collider != null)
        {
            if (boxcastHit.transform.position.x < transform.position.x)
                return true;
            return false;
        }
        return false;
        //return transform.Find("GroundCheck").GetComponent<GroundCheck>().IsGrounded;

        //return Physics2D.OverlapArea(new Vector2(transform.position.x - 0.5f, transform.position.y - 0.48f), new Vector2(transform.position.x + 0.5f, transform.position.y - 0.5f), 1 << 8);
    }


    private bool IsAgainstRightWall()
    {
        float extraWidthTest = .1f;
        RaycastHit2D boxcastHit = Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.right, extraWidthTest, 1 << 8);
        if (boxcastHit.collider != null)
        {
            if (boxcastHit.transform.position.x > transform.position.x)
                return true;
            return false;
        }
        return false;
        //return transform.Find("GroundCheck").GetComponent<GroundCheck>().IsGrounded;

        //return Physics2D.OverlapArea(new Vector2(transform.position.x - 0.5f, transform.position.y - 0.48f), new Vector2(transform.position.x + 0.5f, transform.position.y - 0.5f), 1 << 8);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.TryGetComponent(out Softness soft) && !_closeSofter)
        {
            _closeSofter = soft;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Launcher2D comp))
            comp.Launch(_rb2d, _bidouPressed, _bidouPressed && GameManager.IsInTiming, Options.BidouOptions.BidouLaunchCD);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Softness soft))
        {
            _closeSofter = null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.transform.name);
    }


    public void StartYInteraction(float delay)
    {
        YInteractionJustStarted = true;
        StartCoroutine(StopY(delay));
    }

    IEnumerator StopY(float delay)
    {
        yield return new WaitForSeconds(delay);
        YInteractionJustStarted = false;
    }


}



[Serializable]
public struct PlayerOptions
{
    public float MaxSpeed;
    public float Acceleration;
    public float JumpVelocity;
    public float FallMultiplayer;
    public float JumpMarge;


    public ButtSettings ButtOptions;
    public BidouSettings BidouOptions;
    public SoftnessSettings SoftnessOptions;
    public CheekSettings CheekOptions;
    [Serializable]
    public struct ButtSettings
    {
        public float ButtCD;
        public float ButtMarge;
        public float ButtLength;
        public float ButtGravity;
        public float ButtMaxSpeed;
    }
    [Serializable]
    public struct BidouSettings
    {
        public float BidouCD;
        [Range(0, 1.5f)]
        public float BidouLaunchCD;
        public float BidouMarge;
        [Range(0, 1)]
        public float BidouInputBlockDelay;
    }
    [Serializable]
    public struct SoftnessSettings
    {
        public float SoftnessCD;
        [Range(.25f, 5)]
        public float SoftnessMarge;
        [Range(5, 25)]
        public float AttractionForce;
        public float ThrowForce;
        [Range(0, 1)]
        public float SoftnessInputBlockDelay;
    }

    [Serializable]
    public struct CheekSettings
    {
        public float CheekCD;
        [Range(.25f, 5)]
        public float CheekMarge;
        [Range(5, 25)]
        public float ThrowForce;
        [Range(0, 1)]
        public float CheekInputBlockDelay;
    }
}