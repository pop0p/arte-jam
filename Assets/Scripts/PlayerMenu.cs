﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMenu : MonoBehaviour
{
    private List<Button> _buttons;
    private int _selectedBtns = 0;
    //private float _joystickDelay;
    //private float _btnDelay;
    //// Start is called before the first frame update
    //void Start()
    //{
    //    var x = GameObject.FindGameObjectsWithTag("MenuBtn");
    //    _buttons = x.Select(t => t.GetComponent<Button>()).ToList();
    //    _selectedBtns  = 0;
    //    _buttons[_selectedBtns].Select();
    //    DontDestroyOnLoad(this);
    // }

    //// Update is called once per frame
    //void Update()
    //{
    //    _joystickDelay -= Time.deltaTime;
    //}

    //private void OnEnable()
    //{
    //    _selectedBtns = 0;
    //    var x = GameObject.FindGameObjectsWithTag("MenuBtn");
    //    _buttons = x.Select(t => t.GetComponent<Button>()).ToList();

    //    _buttons[_selectedBtns].Select();
    //}

    private void Start()
    {
        _selectedBtns = 0;
        var x = GameObject.FindGameObjectsWithTag("MenuBtn");
        _buttons = x.Select(t => t.GetComponent<Button>()).ToList();

        _buttons[_selectedBtns].Select();
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        
    }

    private void OnNavigate(InputValue inp) { Debug.Log("^^"); }

    //private void OnNavigate(InputValue inp)
    //{
    //    //if (_joystickDelay > 0) return;
    //    //var x = inp.Get<Vector2>();
    //    //if (x.y != 1 && x.y != -1)
    //    //{
    //    //    _joystickDelay = .1f;
    //    //    return;
    //    //}
    //    //bool valid = false;
    //    //if (x.y == 1)
    //    //{
    //    //    valid = true;
    //    //    Debug.Log("Je monte");
    //    //    if (_selectedBtns == 0)
    //    //        _selectedBtns = _buttons.Count - 1;
    //    //    else
    //    //        _selectedBtns--;

    //    //}
    //    //else if (x.y == -1)
    //    //{
    //    //    valid = true;
    //    //    Debug.Log("Je descend");
    //    //    if (_selectedBtns >= _buttons.Count - 1)
    //    //        _selectedBtns = 0;
    //    //    else
    //    //        _selectedBtns++;
    //    //}
    //    //if (!valid) return;
    //    //_buttons[_selectedBtns].Select();
    //    //Debug.Log(_selectedBtns);
    //    //_joystickDelay = .2f;
    //}

    //private void OnClick(InputValue inp)
    //{
    //    if (inp.isPressed)
    //    {
    //        _buttons[_selectedBtns].onClick.Invoke();
    //    }
    //}
}
